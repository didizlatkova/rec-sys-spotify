# RecSys Challenge - Spotify task
Team Name: FMI_For_Life

## Install dependencies
  * ```pip3 install -r requirements.txt```

## Optional dependencies
  * ```pip3 install ipython``` (interactive python shell)
  * ```pip3 install jupyter```
  * ```pip3 install h5py``` (saving keras models to disk)
  * ```pip3 install autopep8``` (autoformat python code)

## External resources / Prerequisites
  * Add mpd slices to ```data/mpd```
  * Add challenge_set.json file to ```data/challenge```

