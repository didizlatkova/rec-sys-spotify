"""
  Evaluates a given RecSys challenge submision.

  Usage:

        python3 evaluate_submission.py ../data/dev/truth_dev_set.json ../data/results/submission.csv
"""
import sys
import json
import csv
from metrics import aggregate_metrics

NTRACKS = 100

def parse_submission_file(submission_path):
    with open(submission_path, 'r', encoding='utf8') as submission_file:
        reader = csv.reader(submission_file)
        playlists = [r for r in list(reader) if len(r) == NTRACKS + 1]

        return { p[0]: p[1:] for p in playlists }


def evaluate_submission(truth_path, submission_path):
    print('Evaluating...')

    with open(truth_path, encoding='utf8') as truth_file:
        ground_truth = json.load(truth_file)
        sub = parse_submission_file(submission_path)
    
        metrics = aggregate_metrics(ground_truth, sub, NTRACKS, list(ground_truth.keys()))
        for name, value in metrics._asdict().items():
            print(name, '=', value)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("usage: python3 evaluate_submission.py ../data/dev/truth_dev_set.json ../data/results/submission.csv")
        sys.exit()
    evaluate_submission(sys.argv[1], sys.argv[2])
